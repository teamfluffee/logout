package scripts.modules.logout;

import org.tribot.api2007.Login;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.taskmissions.TaskMission;
import scripts.modules.fluffeesapi.scripting.frameworks.task.TaskSet;
import scripts.modules.logout.tasks.LogoutTask;

public class Logout implements TaskMission {

    @Override
    public String getMissionName() {
        return "Logout";
    }

    @Override
    public boolean isMissionValid() {
        return Login.getLoginState() == Login.STATE.INGAME;
    }

    @Override
    public boolean isMissionCompleted() {
        return Login.getLoginState() == Login.STATE.LOGINSCREEN;
    }

    @Override
    public TaskSet getTaskSet() {
        return new TaskSet(new LogoutTask());
    }
}
