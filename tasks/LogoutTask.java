package scripts.modules.logout.tasks;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Login;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSInterfaceChild;
import scripts.modules.fluffeesapi.client.clientextensions.Interfaces;
import scripts.modules.fluffeesapi.scripting.entityselector.finders.prefabs.InterfaceEntity;
import scripts.modules.fluffeesapi.scripting.frameworks.task.Priority;
import scripts.modules.fluffeesapi.scripting.frameworks.task.tasktypes.SuccessTask;
import scripts.modules.fluffeesapi.utilities.Conditions;

public class LogoutTask extends SuccessTask {

    @Override
    public Priority priority() {
        return Priority.MEDIUM;
    }

    @Override
    public boolean isValid() {
        return Login.getLoginState() == Login.STATE.INGAME;
    }

    @Override
    public void successExecute() {
        RSInterfaceChild logoutButton = Interfaces.get(164, 30);
        RSInterface clickHereLogout = new InterfaceEntity().inMaster(182).textEquals("Click here to logout").getFirstResult();
        if (clickHereLogout != null && Interfaces.isInterfaceSubstantiated(clickHereLogout)) {
            clickHereLogout.click();
            Timing.waitCondition(Conditions.loginStateChanged(Login.STATE.INGAME), General.random(3000, 5000));
        } else if (logoutButton != null && Interfaces.isInterfaceSubstantiated(logoutButton)) {
            logoutButton.click("Logout");
            Timing.waitCondition(Conditions.interfaceSubstantiated(182), General.random(3000, 5000));
        }
    }

    @Override
    public String getStatus() {
        return "Logging out";
    }
}